<?php
require_once 'dbutil.php';

/**
 * Description of LearningMaterial
 *
 * @author keio
 */
class LearningMaterial {

    var $materialInfo;
    var $tags;

    function LearningMaterial($id = null) {
        if ($id) {
            $this->materialInfo = getLearningMaterialById($id);
            $this->tags = getTagsByLmId($id);
        }
    }

    function get() {
        return $this->materialInfo;
    }

    function set($name, $abstract, $url) {
        $this->materialInfo['name'] = $name;
        $this->materialInfo['abstract'] = $abstract;
        $this->materialInfo['url'] = $url;
    }

    function saveNew() {
        insertNewLearningMaterial($this->materialInfo['name'], $this->materialInfo['abstract'], $this->materialInfo['url'] = $url);
    }

    function getJson() {
        return json_encode($this->materialInfo);
    }

    function printTags($type = null) {
        if ($this->tags) {
            if ($type == 'edit') {
                foreach ($this->tags as $tag) {
                    echo ('<span id="tag' . $tag['id'] . '"><a class="tag" href="search.php?mode=tag&query=' . $tag['name'] . '">' . $tag['name'] . '</a><a class="tagEditForm" href="#" onclick="removeTag(' . $tag['id'] . ');">削除</a></span>');
                }
            } else {
                foreach ($this->tags as $tag) {
                    echo ('<a class="tag" href="search.php?mode=tag&query=' . $tag['name'] . '">' . $tag['name'] . '</a>');
                }
            }
        }
    }

    function printRankingTr() {
        ?>

        <tr>
            <td class="rankingItem">
                <div class="rankingItemHeader">
                    <div class="rankingItemTitle"><a href="view?lm_id=<?php echo($this->materialInfo['id']); ?>"><?php echo($this->materialInfo['name']); ?></a></div>
                    <div class="rankingItemTag">タグ：<?php $this->printTags(); ?></div>
                </div>
                <div class="rankingItemMain">
                    <?php echo($this->materialInfo['abstract']); ?>
                </div>
                <div class="rankingItemFooter">
                    <div class="rankingItemCount">閲覧数</div>
                </div>
                <hr class="rankingItemHr">
            </td>
        </tr>

        <?php
    }

    function PrintDetail() {
        ?>

        <input type="hidden" id="lm_id" value="<?php echo($this->materialInfo['id']); ?>" />
        <div class="lmHeader">
            <div class="lmTime"><?php echo($this->materialInfo['entry_time']); ?> 投稿</div>
            <div class="lmTitle"><?php echo($this->materialInfo['name']); ?></div>
            <div class="lmTags">タグ：<?php $this->printTags('edit'); ?><a href="#" id="tagEdit">【編集】</a></div>
            <div class="tagEditForm">
                <button id="closeEdit">編集を終わる</button>
                <select id="tags" name="tags">
                    <?php printTagsOptions(); ?>
                </select>
                <button id="submitNewTag">タグを追加</button>
            </div>
        </div>
        <div class="lmMain">
            <?php echo($this->materialInfo['abstract']); ?>
        </div>
        <div class="lmFooter">
            <div class="lmURL"><a href="<?php echo($this->materialInfo['url']); ?>" target="_blank"><?php echo($this->materialInfo['url']); ?></a></div>
        </div>        

        <?php
    }
    
    function printMobile() {
        ?>
        
                <li class="material ui-btn ui-li ui-btn-up-c" data-role="list-divider" data-theme="c">
                    <div class="ui-btn-inner ui-li" aria-hidden="true">
                        <div class="ui-btn-text">
                            <a class="audioViewClick ui-link-inherit" href="#audioView" name="<?php echo($this->materialInfo['url']); ?>" data-rel="dialog">
                                <span class="title"><?php echo($this->materialInfo['name']); ?></span>
                                <span class="comment"><?php echo($this->materialInfo['abstract']); ?></span>
                            </a>
                        </div>
                        <span class="ui-icon ui-icon-arrow-r ui-icon-shadow"></span>
                    </div>
                </li>
        
        <?php
    }

}
?>
