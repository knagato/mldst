<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>教材閲覧</title>
        <style type="text/css" media="screen">
            @import "./css/common.css";
            @import "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/ui-lightness/jquery-ui.css";
            @import "./js/multiselect/ui.multiselect.css";
            @import "./css/view.css";
        </style>
        <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js"></script>
        <script type="text/javascript" src="./js/multiselect/ui.multiselect.js"></script>
        <script type="text/javascript">
            function removeTag(tag_id){
                var lm_id = $("#lm_id").val();
                $.post("./func/ajaxTagUpdater.php", {
                    type: "removeTagWeight",
                    lm_id: lm_id,
                    tag_id: tag_id
                }, function(){
                    $("#tag" + tag_id).remove();
                });
            }
            $(function(){
                $(".multiselect").multiselect();
                $("#tagEdit").bind("click", function(){
                    $(".tagEditForm").css("display", "inline");
                });
                $("#closeEdit").bind("click", function(){
                    $(".tagEditForm").css("display", "none");
                });
                $("#submitNewTag").bind("click", function(){
                    var lm_id = $("#lm_id").val();
                    var tag_id = $("#tags").val();
                    $.post("./func/ajaxTagUpdater.php", {
                        type: "insertTagWeight",
                        lm_id: lm_id,
                        tag_id: tag_id
                    }, function(){
                        var tag_id = $("#tags").val();
                        var tag_name = $("#tags option:selected").text();
                        $("#tagEdit").before('<span id="tag' + tag_id + '"><a class="tag" href="search.php?mode=tag&query=' + tag_name + '">' + tag_name + '</a><a class="tagEditForm" href="#" onclick="removeTag(' + tag_id + ');">削除</a></span>');
                        $(".tagEditForm").css("display", "inline");
                    });
                });
            });
        </script>
    </head>
    <body>
        <?php require './func/header.php'; ?>
        <section id="view">
            <h2>教材を閲覧</h2>

                    <?php
                    if($_GET['lm_id']){
                    require_once './func/LearningMaterial.php';
                        $lm = new LearningMaterial($_GET['lm_id']);
                        $lm->PrintDetail();
                    }else{
                        echo '<td>教材がみつかりません</td>';
                    }
                    ?>


        </section>
        <h4 class="footer">Copyright&nbsp;&copy;&nbsp;1998-2012&nbsp;<a href="http://estudio.sfc.keio.ac.jp/" target="_blank">KEIO&nbsp;SFC&nbsp;スペイン語・スペイン語圏研究室</a></h4>
    </body>
</html>
