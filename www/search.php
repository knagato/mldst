<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mobile Learning Course Designer Street</title>
        <style type="text/css" media="screen">
            @import "./css/common.css";
            @import "./css/search.css";
        </style>
        <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
        <script type="text/javascript">
            $(function(){
                $('input[name="selectMode"]:radio').bind("change", function(){
                    $("#mode").val($(this).val());
                });                
            });
        </script>
    </head>
    <body>
        <?php require './func/header.php'; ?>
        <section id="searchResult">
            <div id="searchBar">
                <h2 id="searchBarTitle">MOBILE LEARN</h2>
                <form method="GET" action="search.php">
                    <input type="hidden" name="mode" id="mode" value="keyword" />
                    <input id="search_query" type="text" name="query" placeholder="教材を検索" value="<?php echo ($_GET['query']); ?>" /><input type="submit" value="検索" />
                </form>
            </div>
            <aside id="sideBar">
                <dl>
                    <dt>検索オプション</dt>
                    <dd><input type="radio" name="selectMode" value="keyword">キーワード</dd>
                    <dd><input type="radio" name="selectMode" value="tag">タグ</dd>
                    <dd><input type="radio" name="selectMode" value="category">カテゴリー</dd>
                    <dd><input type="radio" name="selectMode" value="author">作者</dd>
                </dl>
            </aside>
            <div id="ranking">
                <?php
                require_once 'func/Ranking.php';
                $rank = new Ranking($_GET['query'], $_GET['mode']);
                $rank->printRankingTable();
                ?>
            </div>
        </section>
        <h4 class="footer">Copyright&nbsp;&copy;&nbsp;1998-2012&nbsp;<a href="http://estudio.sfc.keio.ac.jp/" target="_blank">KEIO&nbsp;SFC&nbsp;スペイン語・スペイン語圏研究室</a></h4>
    </body>
</html>
