var clickTrigger = ($.support.touch)? "tap": "click";
$(function(){

    $("#tag").bind(clickTrigger,function(){
        $.post("func/jsdb.php", {
            type: "getTags"
        }, function(result){
            $("#tag_search_subject").empty();
            var $option = $("<option/>");
            $option.text("タグを選択してください。").val("");
            $("#tag_search_subject").append($option);
            for(var i = 0; i < result.length; i++){
                $option = $("<option/>");
                $option.text(result[i].name).val(result[i].name);
                $("#tag_search_subject").append($option);
            }
        }, "json");
    });
    
    $("#tag_search_subject").change(function(){
        $.mobile.showPageLoadingMsg();
        var tag = $("#tag_search_subject option:selected").val();
        $.post("func/jsdb.php", {
            type: "searchByTag",
            tag: tag
        }, function(html){
            $("#tag_search .searchField").html(html);
            $.mobile.hidePageLoadingMsg();            
        }, "html");
    });
    
    $("#keywordSearchSubmit").bind(clickTrigger, function(){
        $.mobile.showPageLoadingMsg();
        var keyword=$("#keywordSearchForm").val();
        $.post("func/jsdb.php", {
            type: "searchByKeyword",
            keyword: keyword
        }, function(html){
            $("#keyword_search .searchField").html(html);
            $.mobile.hidePageLoadingMsg();            
        }, "html");
    });
    
    $("#location").bind(clickTrigger,function(){
        $.mobile.showPageLoadingMsg();
        getLocation();
        $.mobile.hidePageLoadingMsg();
    });
    
    $(".audioViewClick").live(clickTrigger, function(){
        var url = $(this).attr("name");
        var title = $(this).children(".title").html();
        var comment = $(this).children(".comment").html();
        $("#audioTitle").html(title);
        $("#audioComment").html(comment);
        if(judgeBrowser("firefox")){
            url = url.replace(/mp3$/, "ogg");
        }
        $("#audioSource").attr("src",url);
    });
    
    $("#locationSearchButton").bind(clickTrigger, function(){
        var lat = $("#" + gmaplib.id.base_lat).val();
        var lng = $("#" + gmaplib.id.base_lng).val();
        var number = $("#number").val();
        fsqlib.getVenues(lat, lng, number, showVenues);
    });
});

function judgeBrowser(text){
    var strUA = navigator.userAgent.toLowerCase();
    return strUA.indexOf(text) != -1;
}

function getLocation(){
    // 位置情報を獲得
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(show_map, handle_error);
    } else {
        handle_error();
    }
}

function show_map(position){
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    var point = new google.maps.LatLng(lat, lng);
    gmaplib.showMap(point);
    var number = $("#number").val();
    fsqlib.getVenues(lat, lng, number, showVenues);
}

function handle_error(err) {
    alert("geolocation is not supported");
}

function showVenues(result){
    var venues = result.response.groups[0].items
    rankCategories(venues);
    $("#location_exp_select").empty();
    for(var i = 0; i < venues.length; i++){
        var lat = venues[i].location.lat;
        var lng = venues[i].location.lng;
        var info = venues[i].name;
        if(venues[i].categories[0]) info += "(" + venues[i].categories[0].pluralName + ")"
        var $option = $("<option/>").html(info);
        $("#location_exp_select").append($option);
        gmaplib.addMarker(lat, lng, info);
    }
    $("#location_exp_select")[0].selectedIndex = 0;
    $("#location_exp_select").selectmenu("refresh");
}

function rankCategories(venues){
    var result = [];
    function categorySearch(query){
        for(var i = 0; i < result.length; i++){
            if(result[i].name == query){
                return i;
            }
        }
        return -1;
    }
    function addCount(index, name, num){
        if(index > -1){
            result[index].count += num;
        }else{
            result.push({
                name:name,
                count:1
            });
        }        
    }
    for(var i = 0; i < venues.length; i++){
        if(!venues[i].categories[0]) continue;
        var name = venues[i].categories[0].pluralName;
        var parent = venues[i].categories[0].parents[0];
        var index = categorySearch(name);
        addCount(index, name, 1);
        var index2 = categorySearch(parent);
        addCount(index2, parent, 1);
    }
    result.sort(function(a,b){
        return a.count < b.count
    });
    getLM(result);
}

function getLM(ranking){
    $.post("func/jsdb.php", {
        type: "searchByTagsMobile",
        ranking: ranking
    }, function(html){
        $("#location_exp_ul").empty().append(html);
    //            $("#location_search .searchField").html(html);
    }, "html");
}
