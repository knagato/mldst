var gmaplib = {};

gmaplib.id = {
    map: "map_canvas",
    base_lat: "base_lat",
    base_lng: "base_lng"
}

gmaplib.clickTrigger = "click";

gmaplib.baseMarker = new google.maps.Marker({
    draggable: true,
    icon: "img/orange-dot.png"
});

gmaplib.showMap = function (point){
    if(point === undefined) throw new Error("point is undefined");
    gmaplib.map = new google.maps.Map(document.getElementById(gmaplib.id.map),
    {
        zoom:18,
        center: point,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    gmaplib.baseMarker.setPosition(point);
    gmaplib.baseMarker.setMap(gmaplib.map);
    google.maps.event.addListener(gmaplib.baseMarker, "dragend", function(mouseEvent){
        gmaplib.setBasePointForm(mouseEvent.latLng);
    });
}

gmaplib.addMarker = function(lat, lng, info){
    var latlng = new google.maps.LatLng(lat, lng);
    var infoWindow = new google.maps.InfoWindow();
    if(typeof visible !== "boolean") visible = true
    var markerOptions = {
        position:latlng,
        map:gmaplib.map
    }
    var marker = new google.maps.Marker(markerOptions);
    google.maps.event.addListener(marker,gmaplib.clickTrigger,function(){
        infoWindow.setContent(info);
        infoWindow.open(gmaplib.map,marker);
    });
    return marker;
}

gmaplib.setBasePointForm = function(point){
    if(point === undefined) throw new Error("point is undefined");
    document.getElementById(gmaplib.id.base_lat).value = point.lat();
    document.getElementById(gmaplib.id.base_lng).value = point.lng();
}

gmaplib.setBasePointMap = function(point){
    gmaplib.baseMarker.setPosition(point);
    gmaplib.map.setCenter(point);
}

gmaplib.getLatlngByAddress = function(address){
    var geocoder = new google.maps.Geocoder();
    if(address){
        geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latLng = results[0].geometry.location;
                gmaplib.setBasePointForm(latLng);
                gmaplib.setBasePointMap(latLng)
            } else {
                alert("Geocode was not successfull for the following reason: " + status);
            }
        });
    }
    
}