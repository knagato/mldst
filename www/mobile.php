<!DOCTYPE html> 
<html> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <title>Mobile Learn</title> 
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0rc2/jquery.mobile-1.0rc2.min.css" />
        <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.0rc2/jquery.mobile-1.0rc2.min.js"></script> 
        <script src="http://maps.google.co.jp/maps/api/js?sensor=false&language=en" type="text/javascript"></script>
        <script src="./js/gmaplib.js" type="text/javascript" charset="utf-8"></script>
        <script src="./js/fsqlib.js" type="text/javascript" charset="utf-8"></script>
        <script src="./js/mobile.js" type="text/javascript" charset="utf-8"></script>
    </head>
    <body> 

        <div data-role="page" id="home">

            <div data-role="header" data-theme="b">
                <a href="#home" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-left jqm-home">Home</a>
                <h1>Mobile Learn</h1>
                <a href="#help" data-icon="help" data-iconpos="notext" data-direction="reverse" data-rel="dialog" data-transition="pop" class="ui-btn-right">Help</a>
            </div><!-- /header --> 

            <div data-role="content">	
                <ul data-role="listview" data-inset="true" data-theme="c">
                    <li data-role="list-divider">
                        <a id="keyword" href="#keyword_search">キーワードから探す</a>
                    </li>
                    <li data-role="list-divider">
                        <a id="tag" href="#tag_search">タグから探す</a>
                    </li>
                    <li data-role="list-divider">
                        <a id="location" href="#location_search">現在位置から探す</a>
                    </li>
                </ul>
            </div><!-- /content --> 

            <div data-role="footer" data-theme="b"> 
                <h4>Copyright&nbsp;&copy;&nbsp;1998-2011&nbsp;KEIO&nbsp;SFC&nbsp;スペイン語・スペイン語圏研究室</h4> 
            </div><!-- /footer --> 
        </div><!-- /page --> 

        <div data-role="page" id="help">

            <div data-role="header" data-theme="b">
                <h1>Help</h1>
            </div><!-- /header --> 

            <div data-role="content">
                準備中
            </div><!-- /content --> 
        </div><!-- /page --> 

        
        <div data-role="page" id="tag_search">

            <div data-role="header" data-theme="b">
                <a href="#home" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-left jqm-home">Home</a>
                <h1>タグから探す</h1>
                <a href="#help" data-icon="help" data-iconpos="notext" data-direction="reverse" data-rel="dialog" data-transition="pop" class="ui-btn-right">Help</a>
            </div><!-- /header -->

            <div data-role="content">
                <form action="" method="POST">
                    <div class="searchForm" data-role="fieldcontain">
                        <label for="category_search_subject" class="select"></label>
                        <select id="tag_search_subject" data-inline="true">
                            <option value="" selected>カテゴリを選択してください</option>
                        </select>
                    </div>
                </form>
                <div class="searchField">

                </div>
            </div><!-- /content --> 

            <div data-role="footer" data-theme="b"> 
                <h4>Copyright&nbsp;&copy;&nbsp;1998-2010&nbsp;KEIO&nbsp;SFC&nbsp;スペイン語・スペイン語圏研究室</h4> 
            </div><!-- /footer --> 
        </div><!-- /page --> 
        
        <div data-role="page" id="keyword_search"> 

            <div data-role="header" data-theme="b">
                <a href="#home" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-left jqm-home">Home</a>
                <h1>キーワードから探す</h1>
                <a href="#help" data-icon="help" data-iconpos="notext" data-direction="reverse" data-rel="dialog" data-transition="pop" class="ui-btn-right">Help</a>
            </div><!-- /header -->

            <div data-role="content">
                <div class="searchForm">
                    <input id="keywordSearchForm" data-inline="true" type="search" />
                    <a id="keywordSearchSubmit" data-role="button" data-inline="true">検索</a>
                </div>
                <div class="searchField">

                </div>
            </div><!-- /content --> 

            <div data-role="footer" data-theme="b"> 
                <h4>Copyright&nbsp;&copy;&nbsp;1998-2011&nbsp;KEIO&nbsp;SFC&nbsp;スペイン語・スペイン語圏研究室</h4> 
            </div><!-- /footer --> 
        </div><!-- /page --> 
        
        
        <div data-role="page" id="location_search"> 

            <div data-role="header" data-theme="b">
                <a href="#home" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-left jqm-home">Home</a>
                <h1>現在位置から探す</h1>
                <a href="#help" data-icon="help" data-iconpos="notext" data-direction="reverse" data-rel="dialog" data-transition="pop" class="ui-btn-right">Help</a>
            </div><!-- /header -->

            <div data-role="content">
                <div class="searchForm">
                    <div id="map_canvas" style="width: 100%; height: 100px"></div><input id="base_lat" type="hidden" /><input id="base_lng" type="hidden" /><input id="number" type="hidden" value="10" />
                </div>
                <div class="searchField">
                    <div data-role="fieldcontain">
                        <label for="location_exp_select" data-inline="true">
                            現在位置：
                        </label>
                        <select id="location_exp_select">
                        </select>
                        <a id="locationSearchButton" data-role="button" data-inline="true">検索</a>
                    </div>
                    <ul data-role="listview" data-inset="true" data-theme="c" id="location_exp_ul">
                    </ul>
                </div>
            </div><!-- /content --> 
                
            <div data-role="footer" data-theme="b"> 
                <h4>Copyright&nbsp;&copy;&nbsp;1998-2011&nbsp;KEIO&nbsp;SFC&nbsp;スペイン語・スペイン語圏研究室</h4> 
            </div><!-- /footer --> 
        </div><!-- /page --> 

        <div data-role="page" id="audioView">

            <div data-role="header" data-theme="b">
                <h1>教材閲覧</h1>
            </div><!-- /header --> 

            <div data-role="content" data-theme="c">
                <ul data-role="listview" data-inset="true">
                    <li class="material ui-bar-c" data-role="list-divider">
                    <span id="audioTitle"></span>
                    <span id="audioComment"></span>
                    </li>
                    <li class="material ui-bar-c" data-role="list-divider">
                    <audio id="audioSource" controls autobuffer>再生できません</audio>
                    </li>
                </ul>
            </div><!-- /content --> 

            <div data-role="footer" data-theme="b"> 
                <h4>Copyright&nbsp;&copy;&nbsp;1998-2011&nbsp;KEIO&nbsp;SFC&nbsp;スペイン語・スペイン語圏研究室</h4> 
            </div><!-- /footer --> 
        </div><!-- /page --> 
        
    </body> 
</html>