var fsqlib = {};


fsqlib.getVenues = function(lat, lng, number, callback){
    if(!number) number = 10;
    if(!callback) callback = function(result){console.log(result.response.groups[0].items);}
    $.getJSON("js/4sq.json", function(result){
        fsqlib.config = result;
        var baeUrl = fsqlib.config.apiUrl + "/" + fsqlib.config.apiVersion + "/venues/search";
        var latlng = lat.toString() + "," + lng.toString();
        $.get(baeUrl, {
            ll: latlng,
            locale: "en",
            limit: number,
            client_id: fsqlib.config.clientId,
            client_secret: fsqlib.config.clientSecret
        },callback , "json");
    });
}

fsqlib.getVenueCategories = function(callback){
    $.getJSON("js/4sq.json", function(result){
        fsqlib.config = result;
        var baeUrl = fsqlib.config.apiUrl + "/" + fsqlib.config.apiVersion + "/venues/categories";
        $.get(baeUrl, {
            locale: "en",
            client_id: fsqlib.config.clientId,
            client_secret: fsqlib.config.clientSecret
        },callback , "json");
    });
}