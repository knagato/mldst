<?php

class DB {

    var $dbHandle;

    function DB() {
        $this->connect();
    }

    function connect() {
        require 'config.php';
        $this->dbHandle = mysql_connect($dbconf['host'], $dbconf['user'], $dbconf['pass'])
                or die("can not connect db\n");
        mysql_query("SET NAMES utf8", $this->dbHandle);
        mysql_select_db($dbconf['dbname'], $this->dbHandle);
    }
    
    function query($sql) {
        return mysql_query($sql);
    }

    function free($rs) {
        mysql_free_result($rs);
    }

    function close() {
        mysql_close($this->dbHandle);
    }

}

function getLearningMaterials() {
    $result;
    $db = new db();
    $sql = "SELECT lm.id, lm.name, lm.abstract, lm.url, lm.user_id, lm.update_time, lm.entry_time
            FROM mldst_learning_material lm";
    $sql .= " LIMIT 50";
    $rs = $db->query($sql);
    while ($row = mysql_fetch_array($rs)) {
        $result[] = $row;
    }
    $db->free($rs);
    $db->close();
    return $result;
}

function getLearningMaterialsByKeyword($keyword) {
    $keyword = mysql_escape_string($keyword);
    $queries = explode(' ', mb_convert_kana($keyword, 's', 'utf8'));
    $result;
    $db = new db();
    $sql = "SELECT lm.id, lm.name, lm.abstract, lm.url, lm.user_id, lm.update_time, lm.entry_time
            FROM mldst_learning_material lm";
    for ($j = 0; $j < count($queries); $j++) {
        if ($j == 0) {
            $sql .= " WHERE";
        } else {
            $sql .= " AND";
        }
        $sql.= " (lm.name LIKE '%" . $queries[$j] . "%' OR lm.abstract LIKE '%" . $queries[$j] . "%')";
    }
    $sql .= " LIMIT 50";
    $rs = $db->query($sql);
    while ($row = mysql_fetch_array($rs)) {
        $result[] = $row;
    }
    $db->free($rs);
    $db->close();
    return $result;
}

function getLearningMaterialsByTagKeyword($keyword) {
    $keyword = mysql_escape_string($keyword);
    $queries = explode(' ', mb_convert_kana($keyword, 's', 'utf8'));
    $result;
    $db = new db();
    $sql = "SELECT lm.id, lm.name, lm.abstract, lm.url, lm.user_id, lm.update_time, lm.entry_time
            FROM  mldst_tag_weight tw
            INNER JOIN mldst_learning_material lm
            ON tw.lm_id = lm.id
            INNER JOIN mldst_tag t
            ON tw.tag_id = t.id";
    for ($j = 0; $j < count($queries); $j++) {
        if ($j == 0) {
            $sql .= " WHERE";
        } else {
            $sql .= " AND";
        }
        $sql.= " t.name LIKE '%" . $queries[$j] . "%'";
    }
    $sql .= " LIMIT 50";
    $rs = $db->query($sql);
    while ($row = mysql_fetch_array($rs)) {
        $result[] = $row;
    }
    $db->free($rs);
    $db->close();
    return $result;
}

function getLearningMaterialsByScoredTags($ranking){
    for($i = 0; $i < count($ranking); $i++){
        $score = $ranking[$i]["count"];
        $name = $ranking[$i]["name"];
        if($i == 0){
              $query = " (SELECT id, $score score FROM mldst_tag t WHERE t.name = '$name'";
        }else{
            $query .= " UNION SELECT id, $score score FROM mldst_tag t WHERE t.name = '$name'";
        }
    }
    $query .= ") q";
    
    $sql = "SELECT lm.id, lm.name, lm.abstract, lm.url, lm.user_id, lm.update_time, lm.entry_time, q.score * w.weight AS score
FROM $query
JOIN mldst_tag_weight w 
ON q.id = w.tag_id
JOIN mldst_learning_material lm ON w.lm_id = lm.id
ORDER BY score DESC";
    $sql .= " LIMIT 50";
    $db = new db();
    $rs = $db->query($sql);
    while ($row = mysql_fetch_array($rs)) {
        $result[] = $row;
    }
    $db->free($rs);
    $db->close();
    return $result;
}

function getLearningMaterialById($id){
    $id = (int) $id;
    $db = new db();
    $result;
    $sql = "SELECT lm.id, lm.name, lm.abstract, lm.url, lm.user_id, lm.update_time, lm.entry_time
            FROM mldst_learning_material lm
            WHERE lm.id = $id";
    $rs = $db->query($sql);
    $result = mysql_fetch_array($rs);
    $db->free($rs);
    $db->close();
    return $result;
}

function insertNewLearningMaterial($name, $abstract, $url) {
    $name = mysql_escape_string($name);
    $abstract = mysql_escape_string($abstract);
    $url = mysql_escape_string($url);
    $user_id = 1;
    $resultFlag = 0;
    $db = new db();
    $sql = "INSERT INTO mldst_learning_material(name,abstract,url,user_id,entry_time) 
                VALUES('$name',
        '$abstract',
        '$url',
        '$user_id',
        CURRENT_TIMESTAMP)";
    $resultFlag = $db->query($sql);
    $db->close();
    return $resultFlag;
}


function getTags() {
    $result;
    $db = new db();
    $sql = "SELECT t.id, t.name
FROM  mldst_tag t
ORDER BY name";
    $rs = $db->query($sql);
    while ($row = mysql_fetch_array($rs)) {
        $result[] = $row;
    }
    $db->free($rs);
    $db->close();
    return $result;
}

function printTagsOptions(){
    $tags = getTags();
    foreach ($tags as $tag){
        echo ('<option value="' . $tag['id'] . '">' . $tag['name'] . '</option>');
    }
}

function getTagsByLmId($lm_id) {
    $lm_id = (int) $lm_id;
    $result;
    $db = new db();
    $sql = "SELECT t.id, t.name, tw.weight
FROM mldst_tag_weight tw
INNER JOIN mldst_learning_material lm
ON tw.lm_id = lm.id
INNER JOIN mldst_tag t
ON tw.tag_id = t.id
WHERE lm.id = $lm_id";
    $rs = $db->query($sql);
    while ($row = mysql_fetch_array($rs)) {
        $result[] = $row;
    }
    $db->free($rs);
    $db->close();
    return $result;
}

function insertNewTagWeight($lm_id, $tag_id) {
    $lm_id = (int) $lm_id;
    $tag_id = (int) $tag_id;
    $user_id =1;
    $weight = 1;
    $resultFlag = 0;
    $db = new db();
    $sql = "INSERT INTO mldst_tag_weight(lm_id,tag_id,user_id,weight) 
                VALUES('$lm_id',
        '$tag_id',
        '$user_id',
        '$weight')";
    $resultFlag = $db->query($sql);
    $db->close();
    return $resultFlag;
}

function deletetTagWeight($lm_id, $tag_id) {
    $lm_id = (int) $lm_id;
    $tag_id = (int) $tag_id;
    $resultFlag = 0;
    $db = new db();
    $sql = "DELETE FROM mldst_tag_weight WHERE lm_id = $lm_id AND tag_id = $tag_id";
    $resultFlag = $db->query($sql);
    $db->close();
    return $resultFlag;
}

?>
