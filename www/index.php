<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mobile Learning Course Designer Street</title>
        <style type="text/css" media="screen">
            @import "./css/common.css";
            @import "./css/index.css";
        </style>
        <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
        <script src="./js/index.js"></script>
    </head>
    <body>
        <?php require './func/header.php'; ?>
        <section id="search">
            <ul id="search_options">
                <li id="keyword" class="search_option">キーワード</li>
                <li>|</li>
                <li id="tag" class="search_option">タグ</li>
                <li>|</li>
                <li id="author" class="search_option">作者</li>
            </ul>
            <form method="GET" action="search.php">
                <input id="search_mode" type="hidden" name="mode" value="" />
                <input id="search_query" type="text" name="query" placeholder="教材を検索" /><input type="submit" value="検索" />
            </form>
        </section>
    </body>
</html>
