$(function(){
    var point = new google.maps.LatLng(35.680795, 139.766643); // 東京駅;
    gmaplib.showMap(point);
    gmaplib.setBasePointForm(point);
    //    fsqlib.getVenues(point.lat(), point.lng(), showVenues);
    $("#base_name").bind("change", function(){
        gmaplib.getLatlngByAddress($(this).val());
    });
    $("#search").bind("click", function(){
        var lat = $("#" + gmaplib.id.base_lat).val();
        var lng = $("#" + gmaplib.id.base_lng).val();
        var number = $("#number").val();
        fsqlib.getVenues(lat, lng, number, showVenues);
    });
//    fsqlib.getVenueCategories(showAllCategories);
});

function showVenues(result){
    var venues = result.response.groups[0].items
    console.log("venue number:" + venues.length);
    console.log(venues);
    rankCategories(venues);
    for(var i = 0; i < venues.length; i++){
        var lat = venues[i].location.lat;
        var lng = venues[i].location.lng;
        var info = venues[i].name;
        if(venues[i].categories[0]) info += "<br />(" + venues[i].categories[0].pluralName + ")"
        gmaplib.addMarker(lat, lng, info);
    }
}

function showAllCategories(result){
    var categories = result.response.categories;
    for(var i = 0; i < categories.length; i++){
        var subcategories = categories[i].categories;
        for(var j = 0; j < subcategories.length; j ++){
            var $tr = $("<tr/>");
            var $top = $("<td/>");
            var $sub = $("<td/>");
            $top.text(categories[i].pluralName);
            $sub.text(subcategories[j].pluralName);
            $tr.append($top).append($sub);
            $("#category").append($tr);
        }
    }
}

function rankCategories(venues){
    var result = [];
    function categorySearch(query){
        for(var i = 0; i < result.length; i++){
            if(result[i].name == query){
                return i;
            }
        }
        return -1;
    }
    function addCount(index, name, num){
        if(index > -1){
            result[index].count += num;
        }else{
            result.push({
                name:name,
                count:1
            });
        }        
    }
    for(var i = 0; i < venues.length; i++){
        if(!venues[i].categories[0]) continue;
        var name = venues[i].categories[0].pluralName;
        var parent = venues[i].categories[0].parents[0];
        var index = categorySearch(name);
        addCount(index, name, 1);
        var index2 = categorySearch(parent);
        addCount(index2, parent, 1);
    }
    result.sort(function(a,b){return a.count < b.count});
    console.log(result);
    getLM(result);
}

function getLM(ranking){
    $.post("func/jsdb.php", {
        type: "searchByTags",
        ranking: ranking
    }, function(result){
        $("#ranking").empty().append(result);
    }, "html");
}