<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mobile Learning Course Designer Street</title>
        <style type="text/css" media="screen">
            @import "./css/common.css";
            @import "./css/search.css";
            header {
                background-color: black;
                color: white;
            }
            #map_param * {
                display: inline;
                margin: 2px;
                padding-left: 10px;
            }
            select > option {
                display: block;
            }
            #map_canvas {
                height: 200px;
                width: 1200px;
                background-color: wheat;
            }
            input {
                width: 100px;
            }
        </style>
        <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
        <script src="http://maps.google.co.jp/maps/api/js?sensor=false&language=ja&libraries=geometry" type="text/javascript"></script>
        <script src="./js/gmaplib.js" type="text/javascript" charset="utf-8"></script>
        <script src="./js/fsqlib.js" type="text/javascript" charset="utf-8"></script>
        <script src="./js/map.js" type="text/javascript" charset="utf-8"></script>
    </head>
    <body>
        <?php require './func/header.php'; ?>
        <section id="searchResult">
            <div id="searchBar">
                <h2 id="searchBarTitle">MOBILE LEARN</h2>
                <div id="map_param">
                    基準点（緯度：<input id="base_lat" type="number" />, 経度：<input id="base_lng" type="number" />, 地名：<input id="base_name" type="text" value="東京駅" />）&nbsp;
                    粒度(～50)<input id="number" type="number" value="10" />&nbsp;
                    <button id="search">教材を検索</button>
                </div>
            </div>
            <div id="map_canvas"></div>
            <div id="ranking">
            </div>
        </section>
    </body>
</html>
