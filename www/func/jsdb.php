<?php
require_once './dbutil.php';

if($_POST["type"] == 'removeTagWeight'){
    deletetTagWeight($_POST["lm_id"], $_POST["tag_id"]);
    exit ();
}

if($_POST["type"] == 'insertTagWeight'){
    insertNewTagWeight($_POST["lm_id"], $_POST["tag_id"]);
    exit ();
}

if ($_POST["type"] == 'searchByTags') {
    require_once './Ranking.php';
    $rank = new Ranking($_POST["ranking"], "ranking");
    $rank->printRankingTable();
    exit();
}

if ($_POST["type"] == 'searchByTag') {
    require_once './Ranking.php';
    $rank = new Ranking($_POST["tag"], "tag");
    $rank->printMobile();
    exit();
}

if ($_POST["type"] == 'searchByKeyword') {
    require_once './Ranking.php';
    $rank = new Ranking($_POST["keyword"], "keyword");
    $rank->printMobile();
    exit();
}

if ($_POST["type"] == 'searchByTagsMobile') {
    require_once './Ranking.php';
    $rank = new Ranking($_POST["ranking"], "ranking");
    $rank->printMobile();
    exit();
}

if ($_POST["type"] == 'getTags') {
    $tags = getTags();
    echo json_encode($tags);
    exit();
}
?>
