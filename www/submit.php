<?php

if($_POST['name']){
    require_once './func/LearningMaterial.php';
    $lm = new LearningMaterial();
    $lm->set($_POST['name'], $_POST['abstract'], $_POST['url']);
    $lm->saveNew();
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>教材投稿</title>
        <style type="text/css" media="screen">
            @import "./css/common.css";
            @import "./js/jqtransformplugin/jqtransform.css";
            @import "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/ui-lightness/jquery-ui.css";
            @import "./js/multiselect/ui.multiselect.css";
            @import "./css/submit.css";
        </style>
        <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js"></script>
        <script type="text/javascript" src="./js/jqtransformplugin/jquery.jqtransform.js" ></script>
        <script type="text/javascript" src="./js/multiselect/ui.multiselect.js"></script>
        <script type="text/javascript">
            $(function(){
                $('#form').jqTransform({imgPath:'./jqtransformplugin/img/'});
                $(".multiselect").multiselect();
                $('#submit').bind('click', function(){
                    if(!$('#name').val()){
                        window.alert("教材の名前を入力してください。");
                    }else{
                        window.alert("教材を登録しました");
                        $('#form').submit();
                    }
                });
            });
        </script>
    </head>
    <body>
        <?php require './func/header.php'; ?>
        <section>
            <form id="form" method="POST" action="submit.php">
                <div id="basic">
                    <div class="rowElem">
                        <label><h2>タイトル</h2></label>
                    </div>
                    <div class="rowElem">
                        <input id="name" type="text" name="name" />
                    </div>
                    <div class="rowElem">
                        <label><h2>概要</h2></label>
                    </div>
                    <div class="rowElem">
                        <textarea id="abstract" name="abstract"></textarea>
                    </div>
                    <div class="rowElem">
                        <label><h2>URL</h2></label>
                    </div>
                    <div class="rowElem">
                        <input id="url" type="text" name="url" />
                    </div>
                </div>
                <div id="advanced">
                    <div class="rowElem">
                        <label><h2>タグ</h2></label>
                    </div>
                    <div class="rowElem">
                        <select id="tags" class="multiselect" name="tags[]" multiple="multiple">
                            <?php require_once './func/dbutil.php'; printTagsOptions(); ?>
                        </select>
                    </div>
                </div>
                <div class="submit">
                    <button id="submit" />投稿</button>
                </div>
            </form>
        </section>
    </body>
</html>
