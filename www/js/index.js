$(function(){
    setSearchOption($(".search_option:first"));
    
    $(".search_option").bind("click", function(){
        setSearchOption(this);
        var mode = $(this).attr("id");
        $("#search_mode").val(mode);
    });
});

function setSearchOption(target){
    $(".search_option").css("font-weight", "normal").css("color", "blue");
    $(target).css("font-weight", "bold").css("color", "black");
}