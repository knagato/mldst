<?php
require_once 'dbutil.php';

/**
 * Description of Ranking
 *
 * @author keio
 */
class Ranking {

    var $learningMaterials;

    function Ranking($query = null, $mode = null) {
        if ($query) {
            if ($mode == 'tag') {
                $this->learningMaterials = getLearningMaterialsByTagKeyword($query);
            } else if ($mode == 'ranking') {
                $this->learningMaterials = getLearningMaterialsByScoredTags($query);
            } else {
                $this->learningMaterials = getLearningMaterialsByKeyword($query);
            }
        } else {
            $this->learningMaterials = getLearningMaterials();
        }
    }

    function printRankingTable() {
        ?>

        <table>
            <tbody>

                <?php
                require_once 'LearningMaterial.php';
                $materials = $this->learningMaterials ? $this->learningMaterials : getLearningMaterials();
                foreach ($materials as $material) {
                    $lm = new LearningMaterial($material['id']);
                    $lm->printRankingTr();
                }
                ?>

            </tbody>
        </table>

        <?php
    }

    function printMobile() {
        require_once 'LearningMaterial.php';
        $materials = $this->learningMaterials ? $this->learningMaterials : getLearningMaterials();
        if (is_array($materials)) {
            foreach ($materials as $material) {
                $lm = new LearningMaterial($material['id']);
                $lm->printMobile();
            }
        } else {
            echo '<li>教材はありません</li>';
        }
    }

}
?>
