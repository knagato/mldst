<header>
    <ul>
        <li><a href="index.php">トップ</a></li>
        <li>|</li>
        <li><a href="signup.php">新規ユーザ登録</a></li>
        <li>|</li>
        <li><a href="submit.php">教材登録</a></li>
        <li>|</li>
        <li><a href="mypage.php">マイページ</a></li>
        <li>|</li>
        <li><a href="search.php?mode=&query=">教材一覧</a></li>
        <li>|</li>
        <li><a href="map.php">場所から検索</a></li>
        <li>|</li>
        <li><a href="mobile.php">モバイル版</a></li>
    </ul>
</header>
