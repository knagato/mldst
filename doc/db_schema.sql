DROP TABLE IF EXISTS mldst_tag_weight CASCADE;
DROP TABLE IF EXISTS mldst_learning_material CASCADE;
DROP TABLE IF EXISTS mldst_users CASCADE;
DROP TABLE IF EXISTS mldst_tag CASCADE;

CREATE TABLE mldst_users(
	id		BIGINT UNSIGNED AUTO_INCREMENT,
	login_name	VARCHAR(100) UNIQUE,
	name 	TEXT,
	password	TEXT,
	email	TEXT,
	PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE mldst_learning_material(
	id		BIGINT UNSIGNED AUTO_INCREMENT,
	name 	TEXT,
	abstract 	TEXT,
	url		TEXT,
	user_id		BIGINT UNSIGNED,
	update_time	TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	entry_time	TIMESTAMP,
	PRIMARY KEY (id),
	INDEX (user_id),
	FOREIGN KEY (user_id) REFERENCES mldst_users (id)
)ENGINE=InnoDB;

CREATE TABLE mldst_tag(
	id		BIGINT UNSIGNED AUTO_INCREMENT,
	name	TEXT,
	parent_tag_id	BIGINT UNSIGNED,
	meta_tag		TINYINT(1),
	PRIMARY KEY (id),
	INDEX (parent_tag_id),
	FOREIGN KEY (parent_tag_id) REFERENCES mldst_tag (id)
)ENGINE=InnoDB;

CREATE TABLE mldst_tag_weight(
	id		BIGINT UNSIGNED AUTO_INCREMENT,
	name	TEXT,
	lm_id	BIGINT UNSIGNED,
	tag_id	BIGINT UNSIGNED,
	user_id	BIGINT UNSIGNED,
	weight	INTEGER,
	PRIMARY KEY (id),
	INDEX (lm_id),
	FOREIGN KEY (lm_id) REFERENCES mldst_learning_material (id),
	INDEX (tag_id),
	FOREIGN KEY (tag_id) REFERENCES mldst_tag (id),
	INDEX (user_id),
	FOREIGN KEY (user_id) REFERENCES mldst_users (id)
)ENGINE=InnoDB;
